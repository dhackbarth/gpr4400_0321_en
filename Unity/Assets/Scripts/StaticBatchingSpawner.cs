using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticBatchingSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _object;
    [SerializeField] private int _columns = 10;
    [SerializeField] private int _rows = 10;

    private void Start()
    {
        for (int x = 0; x < _columns; x++)
        {
            for (int y = 0; y < _rows; y++)
            {
                Instantiate(_object, new Vector3(x, y, 0.0f), Quaternion.identity, transform);
            }
        }
        
        StaticBatchingUtility.Combine(gameObject);
    }
}
