using UnityEngine;

namespace DefaultNamespace
{
    public static class Vector3Extensions
    {
        public static float Azimuth(this Vector3 vector)
        {
            return Vector3.Angle(Vector3.forward, vector) * Mathf.Sign(vector.x);
        }
    }
}