using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{
    public class DungeonGenerator : MonoBehaviour
    {
        [SerializeField] private Room[] _roomPrefabs;
        [SerializeField] private Room _startRoomPrefab;
        [SerializeField] private int _iterations = 5;
        [SerializeField] private float _iterationDelayTime = 1.0f;

        private IEnumerator Start()
        {
            // instantiating the start prefab
            Room startRoom = Instantiate(_startRoomPrefab, Vector3.zero, Quaternion.identity, transform);

            List<Connector> connectors = new List<Connector>(startRoom.Connectors);
            List<Connector> newConnectors = new List<Connector>();
            List<Room> rooms = new List<Room>();
            rooms.Add(startRoom);
            
            // iterating the maximum amount of iterations specified
            for (int i = 0; i < _iterations; i++)
            {
                // iterating over the connectors
                foreach (Connector connector in connectors)
                {
                    // take one of ConnectionType randomly
                    ConnectionType connectionType = connector.ConnectableTypes.Random();
                    if (connectionType == ConnectionType.None) continue;

                    // take one room prefab matching the ConnectionType randomly
                    Room roomPrefab = _roomPrefabs.Random(room => room.RoomType == connectionType);
                    if (roomPrefab == null) continue;
                    
                    // instantiate room prefab
                    Room newRoom = Instantiate(roomPrefab, transform);

                    // take one connector matching ConnectionType of connectors room
                    Connector newRoomConnector =
                        newRoom.Connectors.Random(c => c.ConnectableTypes.Contains(connector.ParentRoom.RoomType));
                    if (newRoomConnector == null)
                    {
                        Destroy(newRoom.gameObject);
                        continue;
                    }

                    // match the connectors position and rotation
                    Vector3 oldConnectorForward = -connector.transform.forward;
                    float rotationCorrection =
                        oldConnectorForward.Azimuth() - newRoomConnector.transform.forward.Azimuth();
                    newRoom.transform.RotateAround(newRoomConnector.transform.position, Vector3.up, rotationCorrection);
                    newRoom.transform.position += connector.transform.position - newRoomConnector.transform.position;

                    // checking the bounds and discard the room of it intersects with another one
                    bool isColliding = false;
                    foreach (Room room in rooms)
                    {
                        Bounds newRoomBounds = new Bounds(newRoom.transform.position, newRoom.RoomBounds.size * 0.99f);
                        Bounds roomBounds = new Bounds(room.transform.position, room.RoomBounds.size * 0.99f);
                        if (newRoomBounds.Intersects(roomBounds))
                        {
                            Destroy(newRoom.gameObject);
                            isColliding = true;
                            break;
                        }
                    }
                 
                    // add connectors of the new room to the list of new connectors
                    if (!isColliding)
                    {
                        newConnectors.AddRange(newRoom.Connectors.Where(c => c != newRoomConnector));
                        rooms.Add(newRoom);
                    }

                    // wait for next iteration
                    yield return new WaitForSeconds(_iterationDelayTime);
                }
                
                // clean up connectors list and add new connectors
                connectors.Clear();
                connectors.AddRange(newConnectors);
                newConnectors.Clear();
            }
        }
    }
}