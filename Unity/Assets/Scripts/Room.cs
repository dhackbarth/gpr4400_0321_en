using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class Room : MonoBehaviour
    {
        [SerializeField] private ConnectionType _roomType = ConnectionType.None;
        public ConnectionType RoomType => _roomType;

        private Connector[] _connectors;
        public Connector[] Connectors => _connectors;
        
        // public Connector[] Connectors { get; private set; }

        [SerializeField] private Bounds _roomBounds;
        public Bounds RoomBounds => _roomBounds;

        private void Awake()
        {
            _connectors = GetComponentsInChildren<Connector>();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(_roomBounds.center, _roomBounds.size);
        }
    }
}