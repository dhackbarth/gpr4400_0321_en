using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class MazeGenerator : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private int _columns = 99;
    [SerializeField] private int _rows = 99;
    [SerializeField] private float _isWallThreshold = 0.5f;
    [SerializeField] private float _isNeighborWallThreshold = 0.5f;
    [SerializeField] private bool _is2DPopulated = false;

    [Header("3D")]
    [SerializeField] private GameObject _cellPrefab;

    [Header("2D")]
    [SerializeField] private Tilemap _tilemap;
    [SerializeField] private RuleTile _tile;

    private int[] _cells = null;

    private void Start()
    {
        GenerateMaze();
        
        if (_is2DPopulated)
            PopulatingMaze2D();
        else
            PopulatingMaze3D();
    }

    private void GenerateMaze()
    {
        _cells = new int[_columns * _rows];
        int maximumColumn = _columns - 1;
        int maximumRow = _rows - 1;

        for (int x = 0; x <= maximumColumn; x++)
        {
            for (int y = 0; y <= maximumRow; y++)
            {
                int index = CalculateIndex(x, y);
                
                // set outline walls
                if (x == 0 || x == maximumColumn || y == 0 || y == maximumRow)
                {
                    _cells[index] = 1;
                }
                // inspect every second cell
                else if (x % 2 == 0 && y % 2 == 0)
                {
                    // set a wall for actual cell
                    if (Random.value < _isWallThreshold)
                    {
                        _cells[index] = 1;
                        
                        // set one neighbor as a wall
                        int horizontalNeighbor = Random.value < _isNeighborWallThreshold ? 1 :
                            Random.value < _isNeighborWallThreshold ? -1 : 0;
                        // exactly the same as above
                        // if (Random.value < _isNeighborWallThreshold) horizontalNeighbor = 1;
                        // else if (Random.value < _isNeighborWallThreshold) horizontalNeighbor = -1;
                        // else horizontalNeighbor = 0;

                        int verticalNeighbor = horizontalNeighbor != 0 ? 0 :
                            Random.value < _isNeighborWallThreshold ? 1 : -1;

                        Assert.IsFalse(horizontalNeighbor != 0 && verticalNeighbor != 0);
                        Assert.IsFalse(horizontalNeighbor == 0 && verticalNeighbor == 0);
                        _cells[CalculateIndex(x + horizontalNeighbor, y + verticalNeighbor)] = 1;
                    }
                    // not set a wall or do something different
                    else
                    {
                        _cells[index] = 0;
                    }
                }
                // initialize all other cells with 0
                else if (_cells[index] != 1)
                {
                    _cells[index] = 0;
                }
            }
        }
    }

    private void PopulatingMaze3D()
    {
        for (int x = 0; x < _columns; x++)
        {
            for (int y = 0; y < _rows; y++)
            {
                if (_cells[CalculateIndex(x, y)] == 1)
                {
                    GameObject cell = Instantiate(_cellPrefab, new Vector3(x, 0.0f, y), Quaternion.identity, transform);
                    cell.name = $"{x:00} / {y:00}";
                }
            }
        }
    }

    private void PopulatingMaze2D()
    {
        for (int x = 0; x < _columns; x++)
        {
            for (int y = 0; y < _rows; y++)
            {
                if (_cells[CalculateIndex(x, y)] == 0)
                {
                    _tilemap.SetTile(new Vector3Int(x, y, 0), _tile);
                }
            }
        }
    }

    private int CalculateIndex(int x, int y)
    {
        return x * _rows + y;
    }
}
