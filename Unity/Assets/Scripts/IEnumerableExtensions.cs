using System;
using System.Collections.Generic;
using System.Linq;

namespace DefaultNamespace
{
    public static class IEnumerableExtensions
    {
        public static T Random<T>(this IEnumerable<T> collection)
        {
            if (collection == null) return default;

            int count = collection.Count();
            if (count == 0) return default;
            if (count == 1) return collection.First();

            return collection.ElementAt(UnityEngine.Random.Range(0, count));
        }

        public static T Random<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            return collection.Where(predicate).Random();
        }
    }
}