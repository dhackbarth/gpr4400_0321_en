using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParallaxScrolling : MonoBehaviour
{
    [SerializeField] private float _speed = 1.0f;
    [SerializeField] private Vector3 _direction = Vector3.left;
    [SerializeField] private bool _isLooping = true;
    [SerializeField] private bool _isSeamless = true;

    private SpriteRenderer[] _spriteRenderers;
    private Camera _mainCamera;

    private void Start()
    {
        _spriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        // for (int i = 0; i < transform.childCount; i++)
        // {
        //     Transform child = transform.GetChild(i);
        // }

        // foreach (Transform child in transform)
        // {
        //     // also iterating over all children of a transform
        // }
        
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        // move all together, CAUTION: root object position and floating precision
        // transform.position += _direction * (_speed * Time.deltaTime);
        
        // move each tile individually
        Vector3 movement = _direction * (_speed * Time.deltaTime);
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            child.position += movement;
        }

        if (_isLooping && _isSeamless)
        {
            SpriteRenderer firstRenderer = _spriteRenderers[0];
            if (!GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(_mainCamera), firstRenderer.bounds))
            {
                // tile is outside of camera bounds
                SpriteRenderer lastRenderer = _spriteRenderers[_spriteRenderers.Length - 1];
                firstRenderer.transform.localPosition =
                    lastRenderer.transform.localPosition - _direction * firstRenderer.bounds.size.x; // TODO: evaluate correct value based on _direction
                MoveRendererAtTheEnd(_spriteRenderers);
            }
        }
    }

    private void MoveRendererAtTheEnd<T>(T[] array)
    {
        T tmp = array[0];
        for (int i = 0; i < array.Length - 1; i++)
        {
            array[i] = array[i + 1];
        }

        array[array.Length - 1] = tmp;
    }
}
