using System;
using UnityEngine;

namespace DefaultNamespace
{
    public enum ConnectionType
    {
        None,
        Room,
        Corridor,
        EndbossRoom,
        Trove,
        Table,
        Enemy,
        Plant
    }
    
    public class Connector : MonoBehaviour
    {
        [SerializeField] private ConnectionType[] _connectableTypes;
        
        private Room _parentRoom;
        public Room ParentRoom => _parentRoom;
        
        public ConnectionType[] ConnectableTypes => _connectableTypes;

        private void Awake()
        {
            _parentRoom = GetComponentInParent<Room>();
        }
    }
}