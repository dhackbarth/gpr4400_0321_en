using System;
using UnityEngine;

namespace DefaultNamespace
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleExample : MonoBehaviour
    {
        private ParticleSystem _particleSystem;

        private void Awake()
        {
            _particleSystem = GetComponent<ParticleSystem>();

            ParticleSystem.Particle[] particles = new ParticleSystem.Particle[_particleSystem.main.maxParticles];
            int activeParticleCount = _particleSystem.GetParticles(particles);
            for (int i = 0; i < activeParticleCount; i++)
            {
                ParticleSystem.Particle particle = particles[i];
                
            }
        }
    }
}