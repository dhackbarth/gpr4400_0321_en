using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoiseTerrain : MonoBehaviour
{
    [SerializeField] private TerrainData _terrainData;
    [SerializeField] private float _iterations = 1.0f;
    // [SerializeField] private AnimationCurve _topography;
    
    private void Awake()
    {
        int resolution = _terrainData.heightmapResolution;
        float[,] heights = new float[resolution, resolution];

        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                heights[x, y] = Mathf.PerlinNoise(x / (float)resolution * _iterations, y / (float)resolution * _iterations);
            }
        }
        
        _terrainData.SetHeights(0,0, heights);
    }

    // private void Update()
    // {
    //     int resolution = _terrainData.heightmapResolution;
    //     float[,] heights = new float[resolution, resolution];
    //
    //     for (int x = 0; x < resolution; x++)
    //     {
    //         for (int y = 0; y < resolution; y++)
    //         {
    //             heights[x, y] = Mathf.Sin(Time.deltaTime + (float)x / y);
    //         }
    //     }
    //     
    //     _terrainData.SetHeights(0,0, heights);
    // }
}
